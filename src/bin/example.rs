#[macro_use]
extern crate structopt;
use structopt::StructOpt;

#[macro_use]
extern crate log;
extern crate dotenv;

use anyhow::{Context, Result};
use dotenv::dotenv;
use env_logger::Env;
use human_panic::setup_panic;

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about, author)]
struct Opt {
    /// Activate debug mode
    #[structopt(short = "d", long = "debug", env = "DEBUG")]
    debug: bool,
    #[structopt(short = "n", long = "name", env = "NAME")]
    name: String,

    #[structopt(subcommand)]
    command: Option<Command>,
}

#[derive(Debug, StructOpt)]
enum Command {
    One(OneOpts),
    Two(TwoOpts),
}

#[derive(Debug, StructOpt)]
struct OneOpts {
    #[structopt(name = "FST")]
    first: String,
}

#[derive(Debug, StructOpt)]
struct TwoOpts {
    #[structopt(name = "SND")]
    second: String,
}

fn main() -> Result<()> {
    setup_panic!();
    dotenv().ok();
    let env = Env::default().filter_or("LOG_LEVEL", "info");

    env_logger::init_from_env(env);

    let opts = Opt::from_args();
    info!("hello {}", opts.name);
    match opts.command {
        Some(Command::One(opts)) => info!("fst = {}", opts.first),
        Some(Command::Two(opts)) => info!("two = {}", opts.second),
        None => {
            error!("must specify a command!");
            std::process::exit(1);
        }
    };

    Ok(())
}
